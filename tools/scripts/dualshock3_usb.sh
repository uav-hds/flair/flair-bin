#! /bin/bash
ARCH_DIR=$(uname -m)

# $1 is the drone ip
if [ "$#" -gt 2 ] ; then
  echo "Usage: $0 drone_ip_address (drone_port, default 20000)"
  exit 1
fi

# use default port
PORT=${2}
if [ "$#" -eq 1 ] ; then
  PORT="20000"
fi

$FLAIR_ROOT/flair-bin/tools/scripts/dualshock3.sh -a ${1}:$PORT -c usb -t 100 -x ${FLAIR_ROOT}/flair-bin/tools/scripts/dualshock3.xml

